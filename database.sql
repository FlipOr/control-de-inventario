CREATE DATABASE IF NOT EXISTS ctrinv;
USE ctrinv;

CREATE TABLE users(
    id    int(255) auto_increment not null,
    role  varchar(20),
    name  varchar(255),
    surname varchar(255),
    email varchar(255),
    password varchar(255),
    image varchar(255),
    created_at datetime,
    updated_at datetime,
    remember_token varvhar(255),
    CONSTRAINT pk_users PRIMARY KEY(id)
)ENGINE=InnoDb;

CREATE TABLE  media(
    id    int(255) auto_increment not null,
    imagen varchar(255),
    created_at datetime,
    updated_at datetime,
    CONSTRAINT pk_media PRIMARY KEY(id)
)ENGINE=InnoDb;

CREATE TABLE  categorias(
    id    int(255) auto_increment not null,
    nombre varchar(255),
    created_at datetime,
    updated_at datetime,
    CONSTRAINT pk_categoria PRIMARY KEY(id)
)ENGINE=InnoDb;

CREATE TABLE productos(
    id    int(255) auto_increment not null,
    codgigo varchar(255),
    cantidad int(255),
    pasillo varchar(50),
    rack varchar(50),
    nivel int (255),
    descripcion varchar(255),
    categoria_id int(255),
    media_id int(255),
    created_at datetime,
    updated_at datetime,
    CONSTRAINT pk_productos PRIMARY KEY(id),
    CONSTRAINT fk_productos_media FOREIGN KEY(media_id) REFERENCES media(id),
    CONSTRAINT fk_productos_categorias FOREIGN KEY(categoria_id) REFERENCES categorias(id)
)ENGINE=InnoDb;

CREATE TABLE entradas(
    id    int(255) auto_increment not null,
    producto_id int(255),
    provedor varchar(255),
    cantidad int(255),
    pasillo varchar(50),
    nivel int(50),
    rack varchar(50),
    descripcion varchar(255),
    created_at datetime,
    updated_at datetime,
    CONSTRAINT pk_entradas PRIMARY KEY(id),
    CONSTRAINT pk_entradas_productos FOREIGN KEY(producto_id) references productos(id)
)ENGINE=InnoDb;

CREATE TABLE salidas(
    id    int(255) auto_increment not null,
    producto_id int(255),
    provedor varchar(255),
    cantidad int(255),
    pasillo varchar(50),
    nivel int(50),
    rack varchar(50),
    descripcion varchar(255),
    created_at datetime,
    updated_at datetime,
    CONSTRAINT pk_salidas PRIMARY KEY(id),
    CONSTRAINT pk_salidas_productos FOREIGN KEY(producto_id) references productos(id)
)ENGINE=InnoDb;

CREATE TABLE provedores(
    id    int(255) auto_increment not null,
    nombre varchar(255),
    created_at datetime,
    updated_at datetime,
    CONSTRAINT pk_entradas PRIMARY KEY(id)
)ENGINE=InnoDb;