<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Producto;
use App\Media;
use App\Categoria;
use App\Entrada;
use App\Salida;

Route::get('/', function () {
    return view('welcome');
});
//rutas categorias
//main route
Route::get('/categorias', array (
    'as' => 'categorias',
    'middleware' => 'auth',
    'uses' => 'categoriasController@Categorias'
));
//ruta de guardado de categorias
Route::post('/guardar-categoria', array (
    'as' => 'saveCategorias',
    'middleware' => 'auth',
    'uses' => 'categoriasController@saveCategoria'
));
//ruta de borrado de categorias
Route::get('/delete-categoria/{categoria_id}', array (
    'as' => 'deleteCategoria',
    'middleware' => 'auth',
    'uses' => 'categoriasController@deleteCategoria'
));
//ruta de editar categoria
Route::get('/editar-categoria/{categoria_id}', array (
    'as' => 'editCategoria',
    'middleware' => 'auth',
    'uses' => 'categoriasController@editCategoria'
));
//ruta de actualizar categoria
Route::post('/update-categoria/{categoria_id}', array (
    'as' => 'updateCategoria',
    'middleware' => 'auth',
    'uses' => 'categoriasController@updateCategoria'
));

//ruta media
Route::get('/media', array (
    'as' => 'medias',
    'uses' => 'mediasController@medias'
));
//ruta de guardado de medias
Route::post('/guardar-media', array (
    'as' => 'saveMedias',
    'middleware' => 'auth',
    'uses' => 'mediasController@saveMedia'
));
//ruta de borrado de medias
Route::get('/delete-media/{media_id}', array (
    'as' => 'deleteMedia',
    'middleware' => 'auth',
    'uses' => 'mediasController@deleteMedia'
));
//ruta de editar media
Route::get('/editar-media/{media_id}', array (
    'as' => 'editMedia',
    'middleware' => 'auth',
    'uses' => 'mediasController@editMedia'
));
//ruta de actualizar media
Route::post('/update-media/{media_id}', array (
    'as' => 'updateMedia',
    'middleware' => 'auth',
    'uses' => 'mediasController@updateMedia'
));

//ruta acceso visualizacion imagenes
Route::get('/miniatura/{filename}', array(
    'as' => 'imageMedia',
    'uses' => 'mediasController@getImage'
));



//ruta aproductos
Route::get('/agregarproductos', array (
    'as' => 'aproductos',
    'uses' => 'productosController@productos'
));
//ruta aproductos
Route::get('/listadoproductos', array (
    'as' => 'lproductos',
    'uses' => 'productosController@lproductos'
));
//ruta de guardado de productos
Route::post('/guardar-producto', array (
    'as' => 'saveProducto',
    'middleware' => 'auth',
    'uses' => 'productosController@saveProducto'
));
//ruta de eliminacion de productos
Route::get('/delete-producto/{producto_id}', array (
    'as' => 'deleteProducto',
    'middleware' => 'auth',
    'uses' => 'productosController@deleteProducto'
));
//ruta de editar producto
Route::get('/editar-producto/{producto_id}', array (
    'as' => 'editProducto',
    'middleware' => 'auth',
    'uses' => 'productosController@editProducto'
));
//ruta de actualizar producto
Route::post('/update-producto/{producto_id}', array (
    'as' => 'updateProducto',
    'middleware' => 'auth',
    'uses' => 'productosController@updateProducto'
));

//ruta de entradas
Route::get('/entradas', array (
    'as' => 'entradas',
    'uses' => 'entradasController@entradas'
));
//ruta de guardado de entradas
Route::post('/guardar-entrada', array (
    'as' => 'saveEntrada',
    'middleware' => 'auth',
    'uses' => 'entradasController@saveEntrada'
));
//ruta de borrado de entradas
Route::get('/delete-entrada/{entrada_id}', array (
    'as' => 'deleteEntrada',
    'middleware' => 'auth',
    'uses' => 'entradasController@deleteEntrada'
));
//ruta de editar entrada
Route::get('/editar-entrada/{entrada_id}', array (
    'as' => 'editEntrada',
    'middleware' => 'auth',
    'uses' => 'entradasController@editEntrada'
));
//ruta de actualizar producto
Route::post('/update-entrada/{entrada_id}', array (
    'as' => 'updateEntrada',
    'middleware' => 'auth',
    'uses' => 'entradasController@updateEntrada'
));

//ruta de provedores
Route::get('/provedores', array (
    'as' => 'provedores',
    'uses' => 'provedoresController@provedores'
));
//ruta de guardado de provedores
Route::post('/guardar-provedor', array (
    'as' => 'saveProvedor',
    'middleware' => 'auth',
    'uses' => 'provedoresController@saveProvedor'
));
//ruta de borrado de provedores
Route::get('/delete-provedor/{provedor_id}', array (
    'as' => 'deleteProvedor',
    'middleware' => 'auth',
    'uses' => 'provedoresController@deleteProvedor'
));
//ruta de editar provedor
Route::get('/editar-provedor/{provedor_id}', array (
    'as' => 'editProvedor',
    'middleware' => 'auth',
    'uses' => 'provedoresController@editProvedor'
));
//ruta de actualizar provedor
Route::post('/update-provedor/{provedor_id}', array (
    'as' => 'updateProvedor',
    'middleware' => 'auth',
    'uses' => 'provedoresController@updateProvedor'
));

//ruta de salidas
Route::get('/salidas', array (
    'as' => 'salidas',
    'uses' => 'salidasController@salidas'
));
//ruta de guardado de salidas
Route::post('/guardar-salida', array (
    'as' => 'saveSalida',
    'middleware' => 'auth',
    'uses' => 'salidasController@saveSalida'
));
//ruta de borrado de salidas
Route::get('/delete-salida/{salida_id}', array (
    'as' => 'deleteSalida',
    'middleware' => 'auth',
    'uses' => 'salidasController@deleteSalida'
));
//ruta de editar salida
Route::get('/editar-salida/{salida_id}', array (
    'as' => 'editSalida',
    'middleware' => 'auth',
    'uses' => 'salidasController@editSalida'
));
//ruta de actualizar salida
Route::post('/update-salida/{salida_id}', array (
    'as' => 'updateSalida',
    'middleware' => 'auth',
    'uses' => 'salidasController@updateSalida'
));

//ruta de lusuarios
Route::get('/lusuarios', array (
    'as' => 'lusuarios',
    'middleware' => 'auth',
    'uses' => 'userController@users'
));
//ruta de lusuarios
Route::get('/ausuarios', array (
    'as' => 'ausuarios',
    'middleware' => 'auth',
    'uses' => 'userController@auser'
));
//ruta de guardado de usuarios
Route::post('/guardar-usuario', array (
    'as' => 'saveUsuario',
    'middleware' => 'auth',
    'uses' => 'userController@saveUser'
));
//ruta de borrado de usuarios
Route::get('/delete-user/{user_id}', array (
    'as' => 'deleteUser',
    'middleware' => 'auth',
    'uses' => 'userController@deleteUser'
));
//ruta de editar user
Route::get('/editar-user/{user_id}', array (
    'as' => 'editUser',
    'middleware' => 'auth',
    'uses' => 'userController@editUser'
));
//ruta de actualizar salida
Route::post('/update-user/{user_id}', array (
    'as' => 'updateUser',
    'middleware' => 'auth',
    'uses' => 'userController@updateUser'
));

//ruta de genereacion pdf stock
Route::get('pdf', function() {
    $productos = Producto::paginate(1000);
        
        $productos->each(function($productos){
            $productos->categoria;
            $productos->user;
            $productos->entrada;
            $productos->salida;
        });

        $entradas = Entrada::paginate(1000);
        $salidas = Salida::paginate(1000);
        $categorias = Categoria::paginate(1000);
        $medias = Media::paginate(1000);

        foreach($productos as $producto){
            $producto->cantidad;
            foreach($producto->entradas as $entrada){
                $producto->cantidad = $entrada->cantidad + $producto->cantidad;
            }
    
            foreach($producto->salidas as $salida){
                $producto->cantidad = $producto->cantidad - $salida->cantidad;
            }
            $producto->cantidad;
        }
        
        
    $pdf = PDF::loadView('vista', array(
        'productos' => $productos,
        'categorias' => $categorias,
        'medias' => $medias,
        'entradas' => $entradas,
        'salidas' => $salidas
    ));
    $pdf->setPaper('letter', 'landscape');
    return $pdf->download('archivo.pdf');
});

//ruta de generacion de pdf entradas
Route::get('pdfentradas', function() {
    $entradas = Entrada::paginate(1000);
        
        $entradas->each(function($entradas){
            $entradas->producto;
            $entradas->user;
        });

        
        $productos = Producto::paginate(1000);
        $salidas = Salida::paginate(1000);
        $categorias = Categoria::paginate(1000);
        $medias = Media::paginate(1000);

        
        
        
    $pdf = PDF::loadView('pdfentradas', array(
        'productos' => $productos,
        'categorias' => $categorias,
        'medias' => $medias,
        'entradas' => $entradas,
        'salidas' => $salidas
    ));
    $pdf->setPaper('letter', 'landscape');
    return $pdf->download('entradas.pdf');
});

//ruta de generacion de pdf salidas
Route::get('pdfsalidas', function() {
    $salidas = Salida::paginate(1000);
        
        $salidas->each(function($salidas){
            $salidas->producto;
            $salidas->user;
        });

        
        $productos = Producto::paginate(1000);
        $entradas = Entrada::paginate(1000);
        $categorias = Categoria::paginate(1000);
        $medias = Media::paginate(1000);

        
        
        
    $pdf = PDF::loadView('pdfsalidas', array(
        'productos' => $productos,
        'categorias' => $categorias,
        'medias' => $medias,
        'entradas' => $entradas,
        'salidas' => $salidas
    ));
    $pdf->setPaper('letter', 'landscape');
    return $pdf->download('salidas.pdf');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
