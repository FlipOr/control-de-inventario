<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    protected $table = 'productos';
    //Relacion uno a muchos o one to many
    
    public function categoria(){
        return $this->belongsTo('App\Categoria');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function entradas(){
        return $this->hasMany('App\Entrada', 'producto_id');
    }

    public function salidas(){
        return $this->hasMany('App\Salida', 'producto_id');
    }
}
