<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    protected $table = 'categorias';
    //Relacion uno a muchos o one to many
    public function productos(){
        return $this->hasMany('App\Producto');
    }
}
