<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Symfony\component\HttpFoundation\Response;

use App\Producto;
use App\Media;
use App\Categoria;
use App\Entrada;
use App\Provedor;
use Auth;


class entradasController extends Controller
{
    public function Entradas(){
        $entradas = Entrada::paginate(1000);
        $entradas->each(function($entradas){
            $entradas->producto;
            $entradas->user;
        });
        $productos = Producto::paginate(1000);
        $categorias = Categoria::paginate(1000);
        $medias = Media::paginate(1000);
        $provedores = Provedor::paginate(1000);
        
        return view('entradas', array(
            'entradas' => $entradas,
            'productos' => $productos,
            'categorias' => $categorias,
            'medias' => $medias,
            'provedores' => $provedores
        ));
    }

    public function saveEntrada(Request $request){
        //validar formulario 
        $provedores = Provedor::paginate(1000);
        $categorias = Categoria::paginate(1000);
        $entradas = Entrada::paginate(1000);

        $validateData = $this->validate($request, [
            'code' => 'required'
        ]);
        $id = Auth::id();

        $entrada = new Entrada();
        $entrada->codigof = $request->input('code');
        $entrada->producto_id = $request->input('prod');
        $entrada->provedor = $request->input('prov');
        $entrada->cantidad = $request->input('cant');
        $entrada->unidad = $request->input('uni');
        $entrada->descripcion = $request->input('desc');
        $entrada->user_id = $id;

        $entrada->save();

        return redirect()->route('entradas');
    }

    public function deleteEntrada($entrada_id){
        $entradas = Entrada::paginate(1000);
        $productos = Producto::paginate(1000);
        $categorias = Categoria::paginate(1000);
        $medias = Media::paginate(1000);
        $provedores = Provedor::paginate(1000);

        $entrada = Entrada::find($entrada_id);

        $entrada->delete();

        return redirect()->route('entradas');
    }

    public function editEntrada($entrada_id){
        $entradas = Entrada::paginate(1000);
        
        $productos = Producto::paginate(1000);
        $categorias = Categoria::paginate(1000);
        $medias = Media::paginate(1000);
        $provedores = Provedor::paginate(1000);
        
        $entrada = Entrada::find($entrada_id);
        $entrada->each(function($entrada){
            $entrada->producto;
        });


        return view('edit.eentrada', array(
            'entrada' => $entrada,
            'entradas' => $entradas,
            'productos' => $productos,
            'categorias' => $categorias,
            'medias' => $medias,
            'provedores' => $provedores
        ));
    }

    public function updateEntrada($entrada_id, Request $request){
        
        //validarformulario
        $validateData = $this->validate($request, [
        ]);
        $id = Auth::id();
        
        $entrada = Entrada::find($entrada_id);
        
        $entrada->codigof = $request->input('code');
        $entrada->producto_id = $request->input('prod');
        $entrada->provedor = $request->input('prov');
        $entrada->cantidad = $request->input('cant');
        $entrada->unidad = $request->input('uni');
        $entrada->descripcion = $request->input('desc');
        $entrada->user_id = $id;
        
        $entrada->update();


        return redirect()->route('entradas')->with(array(
            'mesage' =>  'La entrada se actualizo correctamente!!'
            
        ));

    }
    
}