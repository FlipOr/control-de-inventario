<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Symfony\component\HttpFoundation\Response;

use App\Producto;
use App\Media;
use App\Categoria;
use App\Entrada;
use App\User;

class userController extends Controller
{
    public function users(){
        $productos = Producto::all();
        $categorias = Categoria::all();
        $medias = Media::all();
        $users = User::all();
        return view('lusuarios', array(
            'productos' => $productos,
            'categorias' => $categorias,
            'users' => $users,
            'medias' => $medias
        ));
    }

    //guardar auser
    public function auser(Request $request){
        $productos = Producto::all();
        $categorias = Categoria::all();
        $medias = Media::all();
        $users = User::all();
        return view('ausuarios', array(
            'productos' => $productos,
            'categorias' => $categorias,
            'users' => $users,
            'medias' => $medias
        ));
    }

    //guardar User
    public function saveUser(Request $request){
        //validar formulario 
        $productos = Producto::all();
        $categorias = Categoria::all();
        $medias = Media::all();
        $users = User::all();

        $validateData = $this->validate($request, [
            'name' => 'required'
        ]);
        
        
        $user = new User();
        $user->name = $request->input('name');
        $user->role = $request->input('role');
        $user->email = $request->input('email');
        $user->password = bcrypt($request->input('password'));
        $user->surname = $request->input('password');
        
        $user->save();

        return redirect()->route('lusuarios');
    }

    ////////////////////////////////
    public function LProductos(){
        $productos = Producto::paginate(1000);
        $productos->each(function($productos){
            $productos->categoria;
            $productos->user;
        });
        $categorias = Categoria::paginate(1000);
        $medias = Media::paginate(1000);
        $entradas = Entrada::paginate(1000);

        

        return view('lproducto', array(
            'productos' => $productos,
            'categorias' => $categorias,
            'medias' => $medias,
            'entradas' => $entradas
        ));
    }

    

    public function deleteUser($user_id){
        $user = User::paginate(10000);

        $user = User::find($user_id);

        $user->delete();

        return redirect()->route('lusuarios');
    }

    public function editUser($user_id){
        $productos = Producto::all();
        $categorias = Categoria::all();
        $medias = Media::all();
        $users = User::all();

        $user = User::find($user_id);
        

        return view('edit.eusuarios', array(
            'categorias' => $categorias,
            'medias' => $medias,
            'users' => $users,
            'user' => $user));
    }

    public function updateUser($user_id, Request $request){
        
        //validarformulario
        $validateData = $this->validate($request, [
        ]);
        
        $user = User::find($user_id);
        
        $user->name = $request->input('name');
        $user->role = $request->input('role');
        $user->email = $request->input('email');
        $user->password = bcrypt($request->input('password'));
        $user->surname = $request->input('password');
        
        $user->update();


        return redirect()->route('lusuarios')->with(array(
            'mesage' =>  'El Usuario se actualizo correctamente!!'
        ));

    }
}