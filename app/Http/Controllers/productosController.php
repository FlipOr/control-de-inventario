<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Symfony\component\HttpFoundation\Response;

use App\Producto;
use App\Media;
use App\Categoria;
use App\Entrada;
use App\Salida;
use Auth;

class productosController extends Controller
{
    public function Productos(){
        $productos = Producto::paginate(1000);
        $categorias = Categoria::paginate(1000);
        $medias = Media::paginate(1000);
        return view('aproductos', array(
            'productos' => $productos,
            'categorias' => $categorias,
            'medias' => $medias
        ));
    }

    //guardar producto
    public function saveProducto(Request $request){
        //validar formulario 
        $productos = Producto::paginate(1000);
        $categorias = Categoria::paginate(1000);
        $medias = Media::paginate(1000);

        $validateData = $this->validate($request, [
            'title' => 'required'
        ]);
        $id = Auth::id();
        
        $producto = new Producto();
        $producto->nombre = $request->input('title');
        $producto->codigo = $request->input('code');
        $producto->descripcion = $request->input('description');
        $producto->categoria_id = $request->input('catg');
        $producto->media = $request->input('med');

        $producto->pasillo = $request->input('pasi');
        $producto->rack = $request->input('rk');
        $producto->nivel = $request->input('nvl');
        $producto->user_id = $id;
        $producto->save();

        return redirect()->route('lproductos');
    }
    ////////////////////////////////
    public function LProductos(){
        $productos = Producto::paginate(1000);
        
        $productos->each(function($productos){
            $productos->categoria;
            $productos->user;
            $productos->entrada;
            $productos->salida;
        });

        $entradas = Entrada::paginate(1000);
        $salidas = Salida::paginate(1000);
        $categorias = Categoria::paginate(1000);
        $medias = Media::paginate(1000);

        foreach($productos as $producto){
            $producto->cantidad;
            foreach($producto->entradas as $entrada){
                $producto->cantidad = $entrada->cantidad + $producto->cantidad;
            }
    
            foreach($producto->salidas as $salida){
                $producto->cantidad = $producto->cantidad - $salida->cantidad;
            }
            $producto->cantidad;
        }
        

        
        

        

        return view('lproducto', array(
            'productos' => $productos,
            'categorias' => $categorias,
            'medias' => $medias,
            'entradas' => $entradas,
            'salidas' => $salidas
        ));
    }

    

    public function deleteProducto($producto_id){
        $producto = Producto::paginate(10000);

        $producto = Producto::find($producto_id);

        $producto->delete();

        return redirect()->route('lproductos');
    }

    public function editProducto($producto_id){
        $categorias = Categoria::paginate(1000);
        $medias = Media::paginate(1000);

        $producto = Producto::find($producto_id);
        $producto->each(function($producto){
            $producto->categoria;
            $producto->user;
        });

        return view('edit.eproducto', array(
            'categorias' => $categorias,
            'medias' => $medias,
            'producto' => $producto));
    }

    public function updateProducto($producto_id, Request $request){
        
        //validarformulario
        $validateData = $this->validate($request, [
        ]);
        
        $producto = Producto::find($producto_id);
        
        $id = Auth::id();
        
        $producto->nombre = $request->input('title');
        $producto->codigo = $request->input('code');
        $producto->descripcion = $request->input('description');
        $producto->categoria_id = $request->input('catg');
        $producto->media = $request->input('med');

        $producto->pasillo = $request->input('pasi');
        $producto->rack = $request->input('rk');
        $producto->nivel = $request->input('nvl');
        $producto->user_id = $id;
        $producto->update();


        return redirect()->route('lproductos')->with(array(
            'mesage' =>  'El producto se actualizo correctamente!!'
            
        ));

    }
}