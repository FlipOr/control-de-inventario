<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Symfony\component\HttpFoundation\Response;


use App\Categoria;


class categoriasController extends Controller
{
    public function Categorias(){
        $categorias = Categoria::paginate(1000);
        return view('categorias', array(
            'categorias' => $categorias
        ));
    }

    public function saveCategoria(Request $request){
        //validar formulario 
        $categorias = Categoria::paginate(1000);

        $validateData = $this->validate($request, [
            'title' => 'required'
        ]);

        $categoria = new Categoria();
        $categoria->nombre = $request->input('title');

        $categoria->save();

        return redirect()->route('categorias');
    }

    public function deleteCategoria($categoria_id){
        $categoria = Categoria::paginate(10000);

        $categoria = Categoria::find($categoria_id);

        $categoria->delete();

        return redirect()->route('categorias');
    }

    public function editCategoria($categoria_id){
        $categoria = Categoria::find($categoria_id);

        return view('edit.ecategoria', array(
            'categoria' => $categoria));
    }

    public function updateCategoria($categoria_id, Request $request){
        
        //validarformulario
        $validateData = $this->validate($request, [
        ]);
        
        $categoria = Categoria::find($categoria_id);
        
        $categoria->nombre = $request->input('title');
        
        $categoria->update();


        return redirect()->route('categorias')->with(array(
            'mesage' =>  'La categoria se actualizo correctamente!!'
            
        ));

    }
    //
}
