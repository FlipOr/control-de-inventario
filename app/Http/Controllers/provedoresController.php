<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Symfony\component\HttpFoundation\Response;


use App\Provedor;
use App\Categoria;

class provedoresController extends Controller
{
    public function Provedores(){
        $provedores = Provedor::paginate(1000);
        $categorias = Categoria::paginate(1000);
        return view('provedores', array(
            'provedores' => $provedores,
            'categorias' => $categorias
        ));
    }

    public function saveProvedor(Request $request){
        //validar formulario 
        $provedores = Provedor::paginate(1000);
        $categorias = Categoria::paginate(1000);

        $validateData = $this->validate($request, [
            'title' => 'required'
        ]);

        $provedor = new Provedor();
        $provedor->nombre = $request->input('title');

        $provedor->save();

        return redirect()->route('provedores');
    }

    public function deleteProvedor($provedor_id){
        $provedor = Provedor::paginate(10000);

        $provedor = Provedor::find($provedor_id);

        $provedor->delete();

        return redirect()->route('provedores');
    }

    public function editProvedor($provedor_id){
        $provedor = Provedor::find($provedor_id);

        return view('edit.eprovedor', array(
            'provedor' => $provedor));
    }

    public function updateProvedor($provedor_id, Request $request){
        
        //validarformulario
        $validateData = $this->validate($request, [
        ]);
        
        $provedor = Provedor::find($provedor_id);
        
        $provedor->nombre = $request->input('title');
        
        $provedor->update();


        return redirect()->route('provedores')->with(array(
            'mesage' =>  'El provedor se actualizo correctamente!!'
            
        ));

    }
    //
}
