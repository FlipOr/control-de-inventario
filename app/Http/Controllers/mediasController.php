<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;


use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;


use App\Media;


class mediasController extends Controller
{
    public function Medias(){
        $medias = Media::paginate(1000);
        return view('media', array(
            'medias' => $medias
        ));
    }

    //funcion para ver imagenes
    /*public function getImage($filename){
        $file = Storage::disk('images')->get($filename);
        return new Response($file, 200);
    }*/
    public function getImage($filename){
        $file = Storage::disk('images')->get($filename);
        return new Response($file, 200);
    }
    

    //funcion para guardar media
    public function saveMedia(Request $request){
        
        $medias = Media::paginate(1000);
        //validar formulario 

        $validateData = $this->validate($request, [
            'title' => 'required',
            'image' => 'mimes:jpeg,bmp,png'
        ]);

        $media = new Media();
        $media->nombre = $request->input('title');

        // Subida de imagen
        $image = $request->file('image');
        if($image){
            $image_path = time().$image->getClientOriginalName();
            \Storage::disk('images')->put($image_path, \File::get($image));

            $media->imagen = $image_path;
        }

        //Recordar cambiar el usuario predeterminado
        $media->save();

        

        return redirect()->route('medias')->with(array(
            'message' => 'La imagen se ha subido correctamente!!'
        ));
    }

    public function deleteMedia($media_id){

        $media = Media::find($media_id);
        
        Storage::disk('images')->delete($media->imagen);

        $media->delete();

        return redirect()->route('medias')->with(array(
            'mesage' =>  'La media se subio correctamente!!'
            
        ));

    }

    public function editMedia($media_id){
        $media = Media::find($media_id);

        return view('edit.emedia', array(
            'media' => $media));
    }

    public function updateMedia($media_id, Request $request){
        
        //validarformulario
        $validateData = $this->validate($request, [
        ]);
        
        $media = Media::find($media_id);
        
        $media->nombre = $request->input('title');
        //subida de imagen
        $image = $request->file('image');
        if($image){
            $image_path = time().$image->getClientOriginalName();
            \Storage::disk('images')->put($image_path, \File::get($image));

            $media->imagen = $image_path;
        }
        $media->update();


        return redirect()->route('medias')->with(array(
            'mesage' =>  'La convocatoria se actualizo correctamente!!'
            
        ));

    }
}
