<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Symfony\component\HttpFoundation\Response;

use App\Producto;
use App\Media;
use App\Categoria;
use App\Entrada;
use App\Provedor;
use App\Salida;
use Auth;

class salidasController extends Controller
{
    public function Salidas(){
        $entradas = Entrada::paginate(1000);
        $productos = Producto::paginate(1000);
        $categorias = Categoria::paginate(1000);
        $medias = Media::paginate(1000);
        $provedores = Provedor::paginate(1000);
        $salidas = Salida::paginate(1000);
        $salidas->each(function($salidas){
            $salidas->producto;
            $salidas->user;
        });
        
        return view('salidas', array(
            'salidas' => $salidas,
            'entradas' => $entradas,
            'productos' => $productos,
            'categorias' => $categorias,
            'medias' => $medias,
            'provedores' => $provedores
        ));
    }

    public function saveSalida(Request $request){
        //validar formulario 
        $entradas = Entrada::paginate(1000);
        $productos = Producto::paginate(1000);
        $categorias = Categoria::paginate(1000);
        $medias = Media::paginate(1000);
        $provedores = Provedor::paginate(1000);
        $salidas = Salida::paginate(1000);

        $validateData = $this->validate($request, [
        ]);
        $id = Auth::id();

        $salida = new Salida();
        $salida->producto_id = $request->input('prod');
        $salida->cantidad = $request->input('cant');
        $salida->user_id = $id;

        $salida->save();

        return redirect()->route('salidas');
    }

    public function deleteSalida($salida_id){
        $entradas = Entrada::paginate(1000);
        $productos = Producto::paginate(1000);
        $categorias = Categoria::paginate(1000);
        $medias = Media::paginate(1000);
        $provedores = Provedor::paginate(1000);
        $salidas = Salida::paginate(1000);

        $salida = Salida::find($salida_id);

        $salida->delete();

        return redirect()->route('salidas');
    }

    public function editSalida($salida_id){
        $entradas = Entrada::paginate(1000);
        $productos = Producto::paginate(1000);
        $categorias = Categoria::paginate(1000);
        $medias = Media::paginate(1000);
        $provedores = Provedor::paginate(1000);
        $salidas = Salida::paginate(1000);
        
        $salida = Salida::find($salida_id);


        return view('edit.esalida', array(
            'salida' => $salida,
            'entradas' => $entradas,
            'productos' => $productos,
            'categorias' => $categorias,
            'medias' => $medias,
            'provedores' => $provedores
        ));
    }

    public function updateSalida($salida_id, Request $request){
        
        //validarformulario
        $validateData = $this->validate($request, [
        ]);
        
        $id = Auth::id();

        $salida = Salida::find($salida_id);
        
        $salida->producto_id = $request->input('prod');
        $salida->cantidad = $request->input('cant');
        $salida->user_id = $id;

        $salida->update();


        return redirect()->route('salidas')->with(array(
            'mesage' =>  'La salida se actualizo correctamente!!'
            
        ));

    }
    
}