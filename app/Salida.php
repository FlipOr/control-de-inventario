<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Salida extends Model
{
    protected $table = 'salidas';
    //Relacion uno a muchos o one to many
    /*public function medias(){
        return $this->hasMany('App\Liga', 'id_torneos');
    }*/
    public function user(){
        return $this->belongsTo('App\User');
    }

    public function producto(){
        return $this->belongsTo('App\Producto');
    } 
}
