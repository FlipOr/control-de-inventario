<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Entrada extends Model
{
    protected $table = 'entradas';
    //Relacion uno a muchos o one to many
    /*public function medias(){
        return $this->hasMany('App\Liga', 'id_torneos');
    }*/
    public function user(){
        return $this->belongsTo('App\User');
    }

    public function producto(){
        return $this->belongsTo('App\Producto');
    } 
}
