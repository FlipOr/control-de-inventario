@extends('layouts.app')

@section('content')
<div id="content-wrapper">
      
    <div class="container-fluid">

      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Listado de Usuarios</a>
        </li>
        <li class="breadcrumb-item active">Overview</li>
      </ol>

      <!-- Icon Cards-->
      
       

      <!-- DataTables Example -->
      @if(Auth::user()->role == "admin")
      <div class="card mb-3">
        <div class="card-header">
          <i class="fas fa-table"></i>
          Usuarios Registrados
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>No.</th>
                  <th>Nombre</th>
                  <th>Rol</th>
                  <th>E-mail</th>
                  <th>Contraseña</th>
                  <th>Creado</th>
                  <th>Ultima modificación</th>
                  @if(Auth::user()->role == "invitado")
                  @else
                  <th>Acciones</th>
                  @endif
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th>No.</th>
                  <th>Nombre</th>
                  <th>Rol</th>
                  <th>E-mail</th>
                  <th>Contraseña</th>
                  <th>Creado</th>
                  <th>Ultima modificación</th>
                  @if(Auth::user()->role == "invitado")
                  @else
                  <th>Acciones</th>
                  @endif
                </tr>
              </tfoot>
              <tbody>
                
                @foreach($users as $user)
                <tr>
                  <td>{{$user->id}}</td>
                  <td>{{$user->name}}</td>
                  <td>{{$user->role}}</td>
                  <td>{{$user->email}}</td>
                  <td>
                    {{$user->surname}}
                  </td>

                  <td>{{$user->created_at}}</td>
                  <td>{{$user->updated_at}}</td>
                 
                  @if(Auth::user()->role == "invitado")

                  @else
                  <td>
                    <button>
                      <a href="{{route('editUser',['user_id' => $user->id])}}" class="text-warning">
                        <i class="fas fa-edit"></i>
                      </a>
                    </button>
                    <button>
                      <a href="#borrarUser{{$user->id}}" role="button" class="text-danger" data-toggle="modal"><i class="fas fa-trash-alt"></i></a> 
                    </button>
                  </td>

                  @endif

                </tr>
                @endforeach

              </tbody>
            </table>
          </div>
        </div>
      </div>
      @else
        <h2>No tienes permitido ingresar a esta sección<h2>
      @endif

    </div>
    <!-- /.container-fluid -->
    @foreach($users as $user)
    <div id="borrarUser{{$user->id}}" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h4 class="modal-title text-dark text-align-left">¿Estás seguro?</h4>
              </div>
              <div class="modal-body">
                  <p class="text-dark">¿Seguro que quieres borrar el usuario?</p>
                  <div class="col-12">
                    {{$user->name}}
                  </div>
                  <p class="text-warning"><small>Si lo borras, nunca podrás recuperarla.</small></p>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                  <a href="{{url('/delete-user/'.$user->id)}}" class="btn btn-outline-light bg-danger">Eliminar</a>
              </div>
            </div>
        </div>
    </div>
    @endforeach

    
@endsection