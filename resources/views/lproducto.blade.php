@extends('layouts.app')

@section('content')
<div id="content-wrapper">
      
    <div class="container-fluid">

      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Listado de Productos</a>
        </li>
        <li class="breadcrumb-item active">Overview</li>
      </ol>

      <!-- Icon Cards-->
      
       

      <!-- DataTables Example -->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fas fa-table"></i>
          Productos en stock
          <button class="pr-1">
            <a href="pdf" class="text-dark text-align-right mx-auto" style="width: 200px;">Generar Reporte de Stock</a>
          </button>
        </div>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                  <a href="pdf" class="btn btn-outline-light bg-secondary">Eliminar</a>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>No.</th>
                  <th>Nombre</th>
                  <th>Media</th>
                  <th>Categoria</th>
                  <th>Codigo</th>
                  <th>Descripción</th>
                  <th>Cantidad</th>
                  <th>Pasillo</th>
                  <th>Rack</th>
                  <th>Nivel</th>
                  <th>Responsable</th>
                  @if(Auth::user()->role == "invitado")
                  @else
                  <th>Acciones</th>
                  @endif
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th>No.</th>
                  <th>Nombre</th>
                  <th>Media</th>
                  <th>Categoria</th>
                  <th>Codigo</th>
                  <th>Descripción</th>
                  <th>Cantidad</th>
                  <th>Pasillo</th>
                  <th>Rack</th>
                  <th>Nivel</th>
                  <th>Responsable</th>
                  @if(Auth::user()->role == "invitado")
                  @else
                  <th>Acciones</th>
                  @endif
                </tr>
              </tfoot>
              <tbody>
                
                @foreach($productos as $producto)
                <tr>
                  <td>{{$producto->id}}</td>
                  <td>{{$producto->nombre}}</td>
                  <td>
                      <img src="{{ url('/miniatura/'.$producto->media)}}" class="img-fluid rounded" width="80" height="80" alt="{{$producto->media}}">
                  </td>

                  <td>
                    {{$producto->categoria->nombre}}
                  </td>

                  <td>{{$producto->codigo}}</td>
                  <td>{!! nl2br(e($producto->descripcion)) !!}</td>
                  <td>
                    {{$producto->cantidad}}
                  </td>
                  <td>{{$producto->pasillo}}</td>
                  <td>{{$producto->rack}}</td>
                  <td>{{$producto->nivel}}</td>
                  <td>
                    {{$producto->user->name}}
                  </td>
                  @if(Auth::user()->role == "invitado")

                  @else
                  <td>
                      <button>
                        <a href="{{route('editProducto',['producto_id' => $producto->id])}}" class="text-warning">
                          <i class="fas fa-edit"></i>
                        </a> 
                      </button>
                      <button>
                        <a href="#borrarProducto{{$producto->id}}" role="button" class="text-danger" data-toggle="modal"><i class="fas fa-trash-alt"></i></a> 
                      </button>
                  </td>

                  @endif

                </tr>
                @endforeach

              </tbody>
            </table>
          </div>
        </div>
        <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
      </div>

    </div>
    <!-- /.container-fluid -->
    @foreach($productos as $producto)
    <div id="borrarProducto{{$producto->id}}" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h4 class="modal-title text-dark text-align-left">¿Estás seguro?</h4>
              </div>
              <div class="modal-body">
                  <p class="text-dark">¿Seguro que quieres borrar el producto?</p>
                  <div class="col-12">
                    {{$producto->name}}
                  </div>
                  <p class="text-warning"><small>Si la borras, nunca podrás recuperarla.</small></p>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                  <a href="{{url('/delete-producto/'.$producto->id)}}" class="btn btn-outline-light bg-danger">Eliminar</a>
              </div>
            </div>
        </div>
    </div>
    @endforeach

    
@endsection