@extends('layouts.app')

@section('content')
<div id="content-wrapper">
      
    <div class="container-fluid">

      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Provedores</a>
        </li>
        <li class="breadcrumb-item active">Overview</li>
      </ol>

      <!-- Icon Cards-->
      
        

      <!-- Area Chart Example-->
      @if(Auth::user()->role == "invitado")
      @else
      <div class="card mb-3">
        <div class="card-header">
        <!-- Buscar icono -->
        <i class="fas fa-clipboard"></i>
          Agregar Provedor
        </div>
        <div class="card-body">
          <form action="{{ url('/guardar-provedor') }}" method="POST" enctype="multipart/form-data" class="">
            {!! csrf_field() !!}
            <label for="title">Nombre del provedor</label>
            <input type="text" class="form-control" id="title" name="title"/>
            <br>
            <button type="submit" class="">
              <i class="fas fa-plus"></i>  
              Agregar
            </button>
          </form>
        </div>
      </div>
      @endif

      <!-- DataTables Example -->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fas fa-table"></i>
          Provedores Actuales</div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>No.</th>
                  <th>Nombre</th>
                  @if(Auth::user()->role == "invitado")
                  @else
                  <th>Acciones</th>
                  @endif
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th>No.</th>
                  <th>Nombre</th>
                  @if(Auth::user()->role == "invitado")
                  @else
                  <th>Acciones</th>
                  @endif
                </tr>
              </tfoot>
              <tbody>
                
                @foreach($provedores as $provedor)
                <tr>
                  <td>{{$provedor->id}}</td>
                  <td>{{$provedor->nombre}}</td>
                  @if(Auth::user()->role == "invitado")
                  @else
                  <td>
                      <button>
                          <a href="{{route('editProvedor',['provedor_id' => $provedor->id])}}" class="text-warning">
                              <i class="fas fa-edit"></i>
                          </a>  
                      </button>
                      <button>
                          <a href="#borrarProvedor{{$provedor->id}}" role="button" class="text-danger" data-toggle="modal"><i class="fas fa-trash-alt"></i></a> 
                      </button>
                  </td>
                  @endif
                </tr>
                @endforeach

              </tbody>
            </table>
          </div>
        </div>
        <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
      </div>

    </div>
    <!-- /.container-fluid -->
    @foreach($provedores as $provedor)
    <div id="borrarProvedor{{$provedor->id}}" class="modal fade">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h4 class="modal-title text-dark text-align-left">¿Estás seguro?</h4>
              </div>
              <div class="modal-body">
                  <p class="text-dark">¿Seguro que quieres borrar el provedor?</p>
                  <div class="col-12">
                      {{$provedor->nombre}}
                  </div>
                  <p class="text-warning"><small>Si lo borras, nunca podrás recuperarlo.</small></p>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                  <a href="{{url('/delete-provedor/'.$provedor->id)}}" class="btn btn-outline-light bg-danger">Eliminar</a>
              </div>
          </div>
      </div>
  </div>
  @endforeach

    
@endsection
