@extends('layouts.app')

@section('content')
<div id="content-wrapper">
      
    <div class="container-fluid">

      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Entradas</a>
        </li>
        <li class="breadcrumb-item active">Overview</li>
      </ol>

      <!-- Icon Cards-->
      
        

      <!-- Area Chart Example-->
      @if(Auth::user()->role == "invitado")
      @else
      <div class="card mb-3">
        <div class="card-header">
        <!-- Buscar icono -->
        <i class="fas fa-clipboard"></i>
          Agregar Entrada
        </div>
        <div class="card-body">
          <form action="{{ url('/guardar-entrada') }}" method="POST" enctype="multipart/form-data" class="">
            {!! csrf_field() !!}
            <label for="code">Codigo FIFO</label>
            <input type="text" class="form-control" id="code" name="code"/>

            <label for="prod">Producto entrante</label>
            <select class="form-control" id="prod" name="prod">
              <option selected>Selecciona el producto entrante...</option>
              @foreach($productos as $producto)
                  <option value="{{$producto->id}}">{{$producto->nombre}}</option>
              @endforeach
            </select>

            <label for="prov">Provedor del producto</label>
            <select class="form-control" id="prov" name="prov">
              <option selected>Selecciona el provedor...</option>
              @foreach($provedores as $provedor)
                  <option value="{{$provedor->nombre}}">{{$provedor->nombre}}</option>
              @endforeach
            </select>
            
            <label for="cant">Cantidad</label>
            <input type="number" class="form-control" id="cant" name="cant" min="1" max="99999999999999999999999999"/>

            <label for="uni">Tipo de unidad</label>
            <select class="form-control" id="uni" name="uni">
              <option selected>Selecciona el tipo de unidad...</option>
                  <option value="ML">ML</option>
                  <option value="PCS">PCS</option>
                  <option value="KG">KG</option>
            </select>

            <label for="desc">Descripción</label>
            <textarea type="text" class="form-control" id="desc" name="desc"></textarea>



            <br>
            <button type="submit" class="">
              <i class="fas fa-plus"></i>  
              Agregar
            </button>
          </form>
        </div>
      </div>
      @endif

      <!-- DataTables Example -->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fas fa-table"></i>
          Entradas Actuales
          <button class="pr-1">
            <a href="pdfentradas" class="text-dark text-align-right mx-auto" style="width: 200px;">Generar Reporte de Entradas</a>
          </button>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>No.</th>
                  <th>Codigo FIFO</th>
                  <th>Producto</th>
                  <th>Provedor</th>
                  <th>Cantidad</th>
                  <th>Fecha de entrada</th>
                  <th>Descripción</th>
                  <th>Responsable</th>
                  @if(Auth::user()->role == "invitado")
                  @else
                  <th>Acciones</th>
                  @endif
                </tr>
              </thead>
              <tfoot>
                <tr>
                    <th>No.</th>
                    <th>Codigo FIFO</th>
                    <th>Producto</th>
                    <th>Provedor</th>
                    <th>Cantidad</th>
                    <th>Fecha de entrada</th>
                    <th>Descripción</th>
                    <th>Responsable</th>
                    @if(Auth::user()->role == "invitado")
                    @else
                    <th>Acciones</th>
                    @endif
                </tr>
              </tfoot>
              <tbody>
                
                @foreach($entradas as $entrada)
                <tr>
                  <td>{{$entrada->id}}</td>
                  <td>{{$entrada->codigof}}</td>
                  <td>{{$entrada->producto->nombre}}</td>
                  <td>{{$entrada->provedor}}</td>
                  <td>{{$entrada->cantidad}} {{$entrada->unidad}}</td>
                  <td>{{$entrada->created_at}}</td>
                  <td>{{$entrada->descripcion}}</td>
                  <td>
                      {{$entrada->user->name}}
                  </td>
                  @if(Auth::user()->role == "invitado")
                  @else
                  <td>
                      <button>
                          <a href="{{route('editEntrada',['entrada_id' => $entrada->id])}}" class="text-warning">
                              <i class="fas fa-edit"></i>
                            </a> 
                      </button>
                      <button>
                          <a href="#borrarEntrada{{$entrada->id}}" role="button" class="text-danger" data-toggle="modal"><i class="fas fa-trash-alt"></i></a>  
                      </button>
                  </td>
                  @endif
                </tr>
                @endforeach

              </tbody>
            </table>
          </div>
        </div>
        <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
      </div>

    </div>
    <!-- /.container-fluid -->
    @foreach($entradas as $entrada)
    <div id="borrarEntrada{{$entrada->id}}" class="modal fade">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h4 class="modal-title text-dark text-align-left">¿Estás seguro?</h4>
              </div>
              <div class="modal-body">
                  <p class="text-dark">¿Seguro que quieres borrar la Entrada?</p>
                  <div class="col-12">
                      {{$entrada->codigof}}
                  </div>
                  <p class="text-warning"><small>Si la borras, nunca podrás recuperarla.</small></p>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                  <a href="{{url('/delete-entrada/'.$entrada->id)}}" class="btn btn-outline-light bg-danger">Eliminar</a>
              </div>
          </div>
      </div>
  </div>
  @endforeach

    
@endsection
