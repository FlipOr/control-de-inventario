<!-- Bootstrap core CSS-->
<link href="{{ url('vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">


<!-- Page level plugin CSS-->
<link href="{{ url('vendor/datatables/dataTables.bootstrap4.css') }}" rel="stylesheet">

<!-- Custom styles for this template-->
<link href="{{ url('css/sb-admin.css') }}" rel="stylesheet">

<h3 style="display: inline-block">Entradas Actuales</h3>
<h5> CA AUTOMOTIVE DURANGO S. DE R.L. DE C.V.</h5>
<h6>Fecha:<?php echo date("d-M-Y"); ?></h6>
          
          
      <!-- DataTables Example -->
      <div class="card mb-3">
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="50%" cellspacing="0">
              <thead>
                <tr>
                  <th>No.</th>
                  <th>Codigo FIFO</th>
                  <th>Producto</th>
                  <th>Provedor</th>
                  <th>Cantidad</th>
                  <th>Fecha de entrada</th>
                  <th>Descripción</th>
                  <th>Responsable</th>
                </tr>
              </thead>
              
              <tbody>
                
                @foreach($entradas as $entrada)
                <tr>
                  <td>{{$entrada->id}}</td>
                  <td>{{$entrada->codigof}}</td>
                  <td>{{$entrada->producto->nombre}}</td>
                  <td>{{$entrada->provedor}}</td>
                  <td>{{$entrada->cantidad}} {{$entrada->unidad}}</td>
                  <td>{{$entrada->created_at}}</td>
                  <td>{{$entrada->descripcion}}</td>
                  <td>
                      {{$entrada->user->name}}
                  </td>
                  
                </tr>
                @endforeach

              </tbody>
            </table>
          </div>
        </div>
      </div>

    
   

   