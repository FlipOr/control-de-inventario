@extends('layouts.app')

@section('content')
<div id="content-wrapper">
      
    <div class="container-fluid">

      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Editar Usuarios</a>
        </li>
        <li class="breadcrumb-item active">Overview</li>
      </ol>

      <!-- Icon Cards-->
      

      <!-- Area Chart Example-->
      @if(Auth::user()->role == "invitado")
      <h2>No tienes permitido ingresar a esta sección<h2>
      @else
      <div class="card mb-3">
        <div class="card-header">
        <!-- Buscar icono -->
        <i class="fas fa-clipboard"></i>
          Editar Usuario
        </div>
        <div class="card-body">

            <form class="form-horizontal" method="POST" action="{{ route('updateUser', ['user_id' => $user->id]) }}">
                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="name" class="col-md-12 control-label">Nombre Completo (Actual: {{ $user->name }})</label>

                    <div class="col-md-6">
                        <input id="name" type="text" class="form-control" name="name" required autofocus>
                    </div>
                </div>

                <div class="form-group{{ $errors->has('role') ? ' has-error' : '' }}">
                    <label for="role" class="col-md-12 control-label">Tipo de Rol (Actual: {{ $user->role }})</label>

                    <div class="col-md-6">
                        
                        <select class="form-control" id="role" name="role">
                            <option selected>Selecciona el tipo de rol...</option>
                                <option value="admin">Administrador</option>
                                <option value="operador">Operador</option>
                                <option value="invitado">Invitado</option>
                                <option value="inactivo">Inactivo</option>
                        </select>
                    </div>
                </div>

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email" class="col-md-12 control-label">Correo electrónico (Actual: {{ $user->email }})</label>

                    <div class="col-md-6">
                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label for="password" class="col-md-12 control-label">Contraseña (Actual: {{ $user->surname }})</label>

                    <div class="col-md-6">
                        <input id="password" type="password" class="form-control" name="password" required>

                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label for="password-confirm" class="col-md-12 control-label">Confirmar Contraseña</label>

                    <div class="col-md-6">
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="">
                            Editar
                        </button>
                    </div>
                </div>
            </form>
            
        </div>
        <div class="card-footer small text-muted">Ultima vez modificado: {{ $user->updated_at }}</div>
      </div>
      @endif
      
      <!-- DataTables Example -->
    </div>
    <!-- /.container-fluid -->

    
@endsection