@extends('layouts.app')

@section('content')
<div id="content-wrapper">
      
    <div class="container-fluid">

      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Agregar Productos</a>
        </li>
        <li class="breadcrumb-item active">Overview</li>
      </ol>

      <!-- Icon Cards-->
      

      <!-- Area Chart Example-->
      <div class="card mb-3">
        <div class="card-header">
        <!-- Buscar icono -->
        <i class="fas fa-clipboard"></i>
          Editar Producto
        </div>
        <div class="card-body">

            <form action="{{ route('updateProducto', ['producto_id' => $producto->id]) }}" method="POST" enctype="multipart/form-data" class="">
            {!! csrf_field() !!}
            <div class="col-12 row">
                <div class="col-6">
                    <label for="title">Nombre del producto</label><p>(Actual: {{$producto->nombre}})</p>
                    <input type="text" class="form-control" id="title" name="title"/>
                    <label for="code">Codigo del producto</label><p>(Actual: {{$producto->codigo}})</p>
                    <input type="text" class="form-control" id="code" name="code"/>
                    <label for="description">Descripcion del producto</label><p>(Actual:<br> {!! nl2br(e($producto->descripcion)) !!})</p>
                    <textarea type="text" class="form-control" id="description" name="description"></textarea>
                    <label for="catg">Categoría del producto</label><p>(Actual: {{$producto->categoria->nombre}})</p>
                    <select class="form-control" id="catg" name="catg">
                    <option selected>Selecciona la Categoría</option>
                    @foreach($categorias as $categoria)
                        <option value="{{$categoria->id}}">{{$categoria->id}}.-{{$categoria->nombre}}</option>
                    @endforeach
                    </select>
                    <label for="med">Imagen del producto</label><p>(Actual: <img src="{{ url('/miniatura/'.$producto->media)}}" class="img-fluid rounded" width="80" height="80" alt="{{$producto->media}}">)</p>
                    <select class="form-control" id="med" name="med">
                    <option selected>Selecciona la imagen</option>
                    @foreach($medias as $media)
                        <option value="{{$media->imagen}}">{{$media->id}}.-{{$media->nombre}}</option>
                    @endforeach
                    </select>
                </div>
                <div class="col-6">
                    <label for="pasi">Pasillo del producto</label><p>(Actual: {{$producto->pasillo}})</p>
                    <input type="text" class="form-control" id="pasi" name="pasi"/>
                    <label for="rk">Rack del producto</label><p>(Actual: {{$producto->rack}})</p>
                    <input type="text" class="form-control" id="rk" name="rk"/>
                    <label for="nvl">Nivel del producto</label><p>(Actual: {{$producto->nivel}})</p>
                    <input type="text" class="form-control" id="nvl" name="nvl"/>
                    <br>
                    <button type="submit" class="col-12">
                        <i class="fas fa-plus"></i>  
                        Editar
                    </button>
                </div>
                
            </div>
        </div>
        <div class="card-footer small text-muted">Ultima vez actualizado {{$producto->updated_at}}</div>
      </div>

      <!-- DataTables Example -->
      

    </div>
    <!-- /.container-fluid -->

    
@endsection
