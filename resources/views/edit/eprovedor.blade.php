@extends('layouts.app')

@section('content')
<div id="content-wrapper">
      
    <div class="container-fluid">

      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Provedores</a>
        </li>
        <li class="breadcrumb-item active">Editar Provedor</li>
      </ol>

      
        

      <!-- Area Chart Example-->
      <div class="card mb-3">
        <div class="card-header">
        <!-- Buscar icono -->
        <i class="fas fa-clipboard"></i>
          Editar Provedor
        </div>
        <div class="card-body">
            <!--<form action="" method="POST" enctype="multipart/form-data" class="">-->
            <form action="{{ route('updateProvedor', ['provedor_id' => $provedor->id]) }}" method="post" enctype="multipart/form-data" class="">
                <div class="col-12 row">
                    <div class="col-6">
                        {!! csrf_field() !!}
                        <label for="title">Nombre de la Provedor (Actual: {{$provedor->nombre}})</label>
                        <input type="text" class="form-control" id="title" name="title"/>
                        <hr>
                        <button type="submit" class="">
                          <i class="fas fa-plus"></i>  
                          Editar Provedor
                        </button>
                    </div>
                    
                    

            </form>
        </div>
        <div class="card-footer small text-muted">Ultima vez actualizado {{$provedor->updated_at}}</div>
      </div>

      

    </div>
    <!-- /.container-fluid -->
    
    
@endsection