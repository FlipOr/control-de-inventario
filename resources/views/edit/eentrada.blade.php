@extends('layouts.app')

@section('content')
<div id="content-wrapper">
      
    <div class="container-fluid">

      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Entradas</a>
        </li>
        <li class="breadcrumb-item active">Editar Entrada</li>
      </ol>

      <!-- Icon Cards-->
      
        

      <!-- Area Chart Example-->
      <div class="card mb-3">
        <div class="card-header">
        <!-- Buscar icono -->
        <i class="fas fa-clipboard"></i>
          Editar Entrada
        </div>
        <div class="card-body">
          <form action="{{ route('updateEntrada', ['entrada_id' => $entrada->id]) }}" method="POST" enctype="multipart/form-data" class="">
            {!! csrf_field() !!}
            <label for="code">Codigo FIFO (Actual:{{$entrada->codigof}}) </label>
            <input type="text" class="form-control" id="code" name="code"/>

            <label for="prod">Producto entrante (Actual:{{$entrada->producto->nombre}})</label>
            <select class="form-control" id="prod" name="prod">
              <option selected>Selecciona el producto entrante...</option>
              @foreach($productos as $producto)
                  <option value="{{$producto->id}}">{{$producto->nombre}}</option>
              @endforeach
            </select>

            <label for="prov">Provedor del producto (Actual:{{$entrada->provedor}})</label>
            <select class="form-control" id="prov" name="prov">
              <option selected>Selecciona el provedor...</option>
              @foreach($provedores as $provedor)
                  <option value="{{$provedor->nombre}}">{{$provedor->nombre}}</option>
              @endforeach
            </select>
            
            <label for="cant">Cantidad (Actual:{{$entrada->cantidad}})</label>
            <input type="number" class="form-control" id="cant" name="cant" min="1" max="99999999999999999999999999"/>

            <label for="uni">Tipo de unidad (Actual:{{$entrada->unidad}})</label>
            <select class="form-control" id="uni" name="uni">
              <option selected>Selecciona el tipo de unidad...</option>
                  <option value="mts">mts</option>
                  <option value="kgs">kgs</option>
            </select>

            <label for="desc">Descripción</label>
            <p>(Actual: <br> {!! nl2br(e($entrada->descripcion)) !!}</p>
            <textarea type="text" class="form-control" id="desc" name="desc"></textarea>



            <br>
            <button type="submit" class="">
              <i class="fas fa-plus"></i>  
              Agregar
            </button>
          </form>
        </div>
      </div>

      

    </div>
    

    
@endsection
