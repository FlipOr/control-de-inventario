@extends('layouts.app')

@section('content')
<div id="content-wrapper">
      
    <div class="container-fluid">

      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Salidas</a>
        </li>
        <li class="breadcrumb-item active">Editar Salida</li>
      </ol>

      <!-- Icon Cards-->
      
        

      <!-- Area Chart Example-->
      <div class="card mb-3">
        <div class="card-header">
        <!-- Buscar icono -->
        <i class="fas fa-clipboard"></i>
          Editar Salida
        </div>
        <div class="card-body">
          <form action="{{ route('updateSalida', ['salida_id' => $salida->id]) }}" method="POST" enctype="multipart/form-data" class="">
            {!! csrf_field() !!}

            <label for="prod">Salida de producto</label>
            <select class="form-control" id="prod" name="prod">
              <option selected>Selecciona el producto que sale...</option>
              @foreach($productos as $producto)
                  <option value="{{$producto->id}}">{{$producto->nombre}}</option>
              @endforeach
            </select>
            
            <label for="cant">Cantidad</label>
            <input type="number" class="form-control" id="cant" name="cant" min="1" max="99999999999999999999999999"/>

            <br>
            <button type="submit" class="">
              <i class="fas fa-plus"></i>  
              Editar
            </button>
          </form>
        </div>
      </div>

      

    </div>
    <!-- /.container-fluid -->
    @foreach($entradas as $entrada)
    <div id="borrarEntrada{{$entrada->id}}" class="modal fade">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h4 class="modal-title text-dark text-align-left">¿Estás seguro?</h4>
              </div>
              <div class="modal-body">
                  <p class="text-dark">¿Seguro que quieres borrar la Entrada?</p>
                  <div class="col-12">
                      {{$entrada->codigof}}
                  </div>
                  <p class="text-warning"><small>Si la borras, nunca podrás recuperarla.</small></p>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                  <a href="{{url('/delete-entrada/'.$entrada->id)}}" class="btn btn-outline-light bg-danger">Eliminar</a>
              </div>
          </div>
      </div>
  </div>
  @endforeach

    
@endsection
