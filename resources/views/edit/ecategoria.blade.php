@extends('layouts.app')

@section('content')
<div id="content-wrapper">
      
    <div class="container-fluid">

      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Categoria</a>
        </li>
        <li class="breadcrumb-item active">Editar Categoria</li>
      </ol>

      
        

      <!-- Area Chart Example-->
      <div class="card mb-3">
        <div class="card-header">
        <!-- Buscar icono -->
        <i class="fas fa-clipboard"></i>
          Editar Categoria
        </div>
        <div class="card-body">
            <!--<form action="" method="POST" enctype="multipart/form-data" class="">-->
            <form action="{{ route('updateCategoria', ['categoria_id' => $categoria->id]) }}" method="post" enctype="multipart/form-data" class="">
                <div class="col-12 row">
                    <div class="col-6">
                        {!! csrf_field() !!}
                        <label for="title">Nombre de la Categoria (Actual: {{$categoria->nombre}})</label>
                        <input type="text" class="form-control" id="title" name="title"/>
                        <hr>
                        <button type="submit" class="">
                          <i class="fas fa-plus"></i>  
                          Editar Categoria
                        </button>
                    </div>
                    
                    

            </form>
        </div>
        <div class="card-footer small text-muted">Ultima vez actualizado {{$categoria->updated_at}}</div>
      </div>

      

    </div>
    <!-- /.container-fluid -->
    
    
@endsection