@extends('layouts.app')

@section('content')
<div id="content-wrapper">
      
    <div class="container-fluid">

      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Media</a>
        </li>
        <li class="breadcrumb-item active">Editar Media</li>
      </ol>

      
        

      <!-- Area Chart Example-->
      <div class="card mb-3">
        <div class="card-header">
        <!-- Buscar icono -->
        <i class="fas fa-clipboard"></i>
          Editar Media
        </div>
        <div class="card-body">
            <!--<form action="" method="POST" enctype="multipart/form-data" class="">-->
            <form action="{{ route('updateMedia', ['media_id' => $media->id]) }}" method="post" enctype="multipart/form-data" class="">
                <div class="col-12 row">
                    <div class="col-6">
                        {!! csrf_field() !!}
                        <label for="title">Nombre de la imagen</label>
                        <input type="text" class="form-control" id="title" name="title"/>
                        <label for="image">Seleccionar imagen</label>
                        <input type="file" class="form-control" id="image" name="image"/>
                        <hr>
                        <button type="submit" class="">
                          <i class="fas fa-plus"></i>  
                          Editar Media
                        </button>
                    </div>
                    <div class="col-6 text-center">
                      <label for="pr">Imagen Actual</label>  
                      <img src="{{ url('/miniatura/'.$media->imagen)}}" class="img-fluid rounded" alt="Responsive image" id="pr">
                    </div>  
                    
                    

            </form>
        </div>
        <div class="card-footer small text-muted">Ultima vez actualizado {{$media->updated_at}}</div>
      </div>

      

    </div>
    <!-- /.container-fluid -->
    
    
@endsection