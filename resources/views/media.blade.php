@extends('layouts.app')

@section('content')
<div id="content-wrapper">
      
    <div class="container-fluid">

      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Media</a>
        </li>
        <li class="breadcrumb-item active">Overview</li>
      </ol>

      
        

      <!-- Area Chart Example-->
      <!-- Comprovacion de rol -->
      @if(Auth::user()->role == "invitado")
      @else
      <div class="card mb-3">
        <div class="card-header">
        <!-- Buscar icono -->
        <i class="fas fa-clipboard"></i>
          Agregar Media
        </div>
        <div class="card-body">
            <form action="{{ url('/guardar-media') }}" method="POST" enctype="multipart/form-data" class="">
              {!! csrf_field() !!}
              <label for="title">Nombre de la imagen</label>
              <input type="text" class="form-control" id="title" name="title"/>

              <label for="image">Seleccionar imagen</label>
              <input type="file" class="form-control" id="image" name="image"/>
              <hr>
              <button type="submit" class="">
                <i class="fas fa-plus"></i>  
                Agregar
              </button>
            </form>
        </div>
      </div>
      @endif

      <!-- DataTables Example -->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fas fa-table"></i>
          Media Almacenada</div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>No.</th>
                  <th>Nombre</th>
                  <th>Imagen</th>
                  @if(Auth::user()->role == "invitado")
                  @else
                  <th>Acciones</th>
                  @endif
                </tr>
              </thead>
              <tfoot>
                <tr>
                    <th>No.</th>
                    <th>Nombre</th>
                    <th>Imagen</th>
                    @if(Auth::user()->role == "invitado")
                    @else
                    <th>Acciones</th>
                    @endif
                </tr>
              </tfoot>
              <tbody>
                
                  @foreach($medias as $media)
                  <tr>
                    <td>{{$media->id}}</td>
                    <td>{{$media->nombre}}</td>
                    <td>
                        <img src="{{ url('/miniatura/'.$media->imagen)}}" class="img-fluid rounded" width="80" height="80" alt="{{$media->imagen}}">
                    </td>
                    @if(Auth::user()->role == "invitado")
                    @else
                    <td>
                        <button>
                            <a href="{{route('editMedia',['media_id' => $media->id])}}" class="text-warning">
                              <i class="fas fa-edit"></i>
                            </a>
                        </button>
                        <button>
                          <a href="#borrarMedia{{$media->id}}" role="button" class="text-danger" data-toggle="modal"><i class="fas fa-trash-alt"></i></a>  
                        </button>
                    </td>
                    @endif
                  </tr>
                  @endforeach

              </tbody>
            </table>
          </div>
        </div>
        <div class="card-footer small text-muted">
        
        </div>
      </div>

    </div>
    <!-- /.container-fluid -->
    <!--modal eliminar media-->
    @foreach($medias as $media)
    <div id="borrarMedia{{$media->id}}" class="modal fade">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h4 class="modal-title text-dark text-align-left">¿Estás seguro?</h4>
              </div>
              <div class="modal-body">
                  <p class="text-dark">¿Seguro que quieres borrar la media?</p>
                  <div class="col-12">
                      <img src="{{ url('/miniatura/'.$media->imagen)}}" class="img-fluid rounded" alt="Responsive image">
                  </div>
                  <p class="text-warning"><small>Si lo borras, nunca podrás recuperarlo.</small></p>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                  <a href="{{url('/delete-media/'.$media->id)}}" class="btn btn-outline-light bg-danger">Eliminar</a>
              </div>
          </div>
      </div>
  </div>
  @endforeach
  <!--////modal eliminar media-->
    
@endsection