@extends('layouts.app')

@section('content')
<div id="content-wrapper">
      
    <div class="container-fluid">

      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Salidas</a>
        </li>
        <li class="breadcrumb-item active">Overview</li>
      </ol>

      <!-- Icon Cards-->
      
        

      <!-- Area Chart Example-->
      @if(Auth::user()->role == "invitado")
      @else
      <div class="card mb-3">
        <div class="card-header">
        <!-- Buscar icono -->
        <i class="fas fa-clipboard"></i>
          Agregar Salida
        </div>
        <div class="card-body">
          <form action="{{ url('/guardar-salida') }}" method="POST" enctype="multipart/form-data" class="">
            {!! csrf_field() !!}

            <label for="prod">Salida de producto</label>
            <select class="form-control" id="prod" name="prod">
              <option selected>Selecciona el producto que sale...</option>
              @foreach($productos as $producto)
                  <option value="{{$producto->id}}">{{$producto->nombre}}</option>
              @endforeach
            </select>
            
            <label for="cant">Cantidad</label>
            <input type="number" class="form-control" id="cant" name="cant" min="1" max="99999999999999999999999999"/>

            <br>
            <button type="submit" class="">
              <i class="fas fa-plus"></i>  
              Agregar
            </button>
          </form>
        </div>
      </div>
      @endif

      <!-- DataTables Example -->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fas fa-table"></i>
          Salidas Actuales
            <button class="pr-1">
              <a href="pdfsalidas" class="text-dark text-align-right mx-auto" style="width: 200px;">Generar Reporte de Salidas</a>
            </button>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>No.</th>
                  <th>Producto</th>
                  <th>Cantidad</th>
                  <th>Fecha de salida</th>
                  <th>Responsable</th>
                  @if(Auth::user()->role == "invitado")
                  @else
                  <th>Acciones</th>
                  @endif
                </tr>
              </thead>
              <tfoot>
                <tr>
                    <th>No.</th>
                    <th>Producto</th>
                    <th>Cantidad</th>
                    <th>Fecha de entrada</th>
                    <th>Responsable</th>
                    @if(Auth::user()->role == "invitado")
                    @else
                    <th>Acciones</th>
                    @endif
                </tr>
              </tfoot>
              <tbody>
                
                @foreach($salidas as $salida)
                <tr>
                  <td>{{$salida->id}}</td>
                  <td>{{$salida->producto->nombre}}</td>
                  <td>{{$salida->cantidad}} {{$salida->unidad}}</td>
                  <td>{{$salida->created_at}}</td>
                  <td>
                      {{$salida->user->name}}
                  </td>
                  @if(Auth::user()->role == "invitado")
                  @else
                  <td>
                      <button>
                          <a href="{{route('editSalida',['salida_id' => $salida->id])}}" class="text-warning">
                              <i class="fas fa-edit"></i>
                            </a> 
                      </button>
                      <button>
                          <a href="#borrarSalida{{$salida->id}}" role="button" class="text-danger" data-toggle="modal"><i class="fas fa-trash-alt"></i></a>  
                      </button>
                  </td>
                  @endif
                </tr>
                @endforeach

              </tbody>
            </table>
          </div>
        </div>
        <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
      </div>

    </div>
    <!-- /.container-fluid -->
    @foreach($salidas as $salida)
    <div id="borrarSalida{{$salida->id}}" class="modal fade">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h4 class="modal-title text-dark text-align-left">¿Estás seguro?</h4>
              </div>
              <div class="modal-body">
                  <p class="text-dark">¿Seguro que quieres borrar la salida?</p>
                  <div class="col-12">
                      {{$salida->producto->nombre}} {{$salida->cantidad}} {{$salida->created_at}}
                  </div>
                  <p class="text-warning"><small>Si la borras, nunca podrás recuperarla.</small></p>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                  <a href="{{url('/delete-salida/'.$salida->id)}}" class="btn btn-outline-light bg-danger">Eliminar</a>
              </div>
          </div>
      </div>
  </div>
  @endforeach

    
@endsection
