<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Control de Inventario</title>
    <link rel="shortcut icon" href="imgs/logo.ico" />

    <!-- Styles -->
    <!-- Bootstrap core CSS-->
    <link href="{{ url('vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Custom fonts for this template-->
    <link href="{{ url('vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">

    <!-- Page level plugin CSS-->
    <link href="{{ url('vendor/datatables/dataTables.bootstrap4.css') }}" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="{{ url('css/sb-admin.css') }}" rel="stylesheet">
</head>
<body id="page-top">
    <div id="app">
        <nav class="navbar navbar-expand navbar-dark bg-dark static-top">
            
            <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">
              <i class="fas fa-bars"></i>
            </button>

            <a class="navbar-brand mr-1" href="index.html">Control de Inventario</a>
      
            
      
            
            <!-- Navbar -->
            <ul class="navbar-nav d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0>
              
              <li class="nav-item dropdown no-arrow">
                <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        {{ Auth::user()->name }} <span class="caret"></span><i class="fas fa-user-circle fa-fw"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                  <a class="dropdown-item" href="#">Settings</a>
                  <a class="dropdown-item" href="#">Activity Log</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="{{ route('logout') }}"
                      onclick="event.preventDefault();
                               document.getElementById('logout-form').submit();">
                          
                      
                      <span><i class="fas fa-sign-out-alt"></i>Cerrar sesión</span>
                  </a>
                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                      {{ csrf_field() }}
                  </form>
                </div>
              </li>
            </ul>
      
          </nav>

          <div id="wrapper">

            <!-- Sidebar -->
            <ul class="sidebar navbar-nav">
              <li class="nav-item active">
                <a class="nav-link" href="{{ url('/home') }}">
                  <i class="fas fa-fw fa-tachometer-alt"></i>
                  <span>Panel de Control</span>
                </a>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="fas fa-fw fa-cubes"></i>
                  <span>Productos</span>
                </a>
                <div class="dropdown-menu" aria-labelledby="pagesDropdown">
                  <a class="dropdown-item" href="{{ url('/listadoproductos') }}">
                  <i class="fas fa-fw fa-dolly"></i>
                  <span>Admin. Productos</span>
                  </a>
                  @if(Auth::user()->role == "invitado")
                  @else
                  <a class="dropdown-item" href="{{ url('/agregarproductos') }}">
                  <i class="fas fa-fw fa-truck-loading"></i>
                  <span>Agregar Productos</span>
                  </a>
                  @endif
                </div>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{ url('/categorias') }}">
                  <i class="fas fa-fw fa-list"></i>
                  <span>Categorías</span></a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{ url('/media') }}">
                  <i class="fas fa-fw fa-images"></i>
                  <span>Media</span></a>
              </li>
              <li class="nav-item">
                  <a class="nav-link" href="{{ url('/provedores') }}">
                    <i class="fas fa-fw fa-shipping-fast"></i>
                    <span>Provedores</span></a>
                </li>
              <li class="nav-item">
                <a class="nav-link" href="{{ url('/salidas') }}">
                  <i class="fas fa-fw fa-outdent"></i>
                  <span>Salidas</span></a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{ url('/entradas') }}">
                  <i class="fas fa-fw fa-indent"></i>
                  <span>Entradas</span></a>
              </li>
              @if(Auth::user()->role == "admin")
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="fas fa-fw fa-users"></i>
                  <span>Usuarios</span>
                </a>
                <div class="dropdown-menu" aria-labelledby="pagesDropdown">
                  <a class="dropdown-item" href="{{ url('/lusuarios') }}">
                  <i class="fas fa-fw fa-users-cog"></i>
                  <span>Admin. Usuarios</span>
                  </a>
                  <a class="dropdown-item" href="{{ url('/ausuarios') }}">
                  <i class="fas fa-fw fa-user-plus"></i>
                  <span>Agregar Usuario</span>
                  </a>
                </div>
              </li>
              @endif
            </ul>
      
            
      

        @yield('content')

              <!-- Sticky Footer -->
              <footer class="sticky-footer">
                <div class="container my-auto">
                  <div class="copyright text-center my-auto">
                    <span>Copyright © CA Automotive 2018</span>
                    
                  </div>
                </div>
              </footer>
      
            </div>
            <!-- /.content-wrapper -->
    </div>
</div>
<!-- /#wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
  <i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
      <div class="modal-footer">
        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
        <a class="btn btn-primary" href="login.html">
          Logout
          
        </a>
      </div>
    </div>
  </div>
</div>


<!-- Bootstrap core JavaScript-->
<script src="{{ url('vendor/jquery/jquery.min.js') }}"></script>
<script src="{{ url('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

<!-- Core plugin JavaScript-->
<script src="{{ url('vendor/jquery-easing/jquery.easing.min.js') }}"></script>

<!-- Page level plugin JavaScript-->
<script src="{{ url('vendor/chart.js/Chart.min.js') }}"></script>
<script src="{{ url('vendor/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ url('vendor/datatables/dataTables.bootstrap4.js') }}"></script>

<!-- Custom scripts for all pages-->
<script src="{{ url('js/sb-admin.min.js') }}"></script>

<!-- Demo scripts for this page-->
<!-- Traer los demas scripts de las graficas de ser necesario -->
<script src="{{ url('js/demo/datatables-demo.js') }}"></script>
<script src="{{ url('js/demo/chart-area-demo.js') }}"></script>
<script src="{{ url('js/demo/chart-bar-demo.js') }}"></script>
<script src="{{ url('js/demo/chart-pie-demo.js') }}"></script>
    
    
</body>
</html>
