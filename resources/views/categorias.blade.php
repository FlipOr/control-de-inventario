@extends('layouts.app')

@section('content')
<div id="content-wrapper">
      
    <div class="container-fluid">

      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Categorías</a>
        </li>
        <li class="breadcrumb-item active">Overview</li>
      </ol>

      <!-- Icon Cards-->
      
        

      <!-- Area Chart Example-->
      @if(Auth::user()->role == "inactivo")
      <h2>Has sido bloqueado ponte en contacto con tu superior<h2>
      
      @elseif(Auth::user()->role == "invitado")
      @else
      <div class="card mb-3">
        <div class="card-header">
        <!-- Buscar icono -->
        <i class="fas fa-clipboard"></i>
          Agregar Categoría
        </div>
        <div class="card-body">
          <form action="{{ url('/guardar-categoria') }}" method="POST" enctype="multipart/form-data" class="">
            {!! csrf_field() !!}
            <label for="title">Nombre de la Categoría</label>
            <input type="text" class="form-control" id="title" name="title"/>
            <button type="submit" class="">
              <i class="fas fa-plus"></i>  
              Agregar
            </button>
          </form>
        </div>
        <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
      </div>
      @endif

      <!-- DataTables Example -->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fas fa-table"></i>
          Categorias Actuales</div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>No.</th>
                  <th>Nombre</th>
                  @if(Auth::user()->role == "invitado")
                  @else
                  <th>Acciones</th>
                  @endif
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th>No.</th>
                  <th>Nombre</th>
                  @if(Auth::user()->role == "invitado")
                  @else
                  <th>Acciones</th>
                  @endif
                </tr>
              </tfoot>
              <tbody>
                
                @foreach($categorias as $categoria)
                <tr>
                  <td>{{$categoria->id}}</td>
                  <td>{{$categoria->nombre}}</td>
                  @if(Auth::user()->role == "invitado")
                  @else
                  <td>
                      <button>
                          <a href="{{route('editCategoria',['categoria_id' => $categoria->id])}}" class="text-warning">
                              <i class="fas fa-edit"></i>
                          </a>  
                      </button>
                      <button>
                          <a href="#borrarCategoria{{$categoria->id}}" role="button" class="text-danger" data-toggle="modal"><i class="fas fa-trash-alt"></i></a> 
                      </button>
                  </td>
                  @endif
                </tr>
                @endforeach

              </tbody>
            </table>
          </div>
        </div>
        <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
      </div>

    </div>
    <!-- /.container-fluid -->
    @foreach($categorias as $categoria)
    <div id="borrarCategoria{{$categoria->id}}" class="modal fade">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h4 class="modal-title text-dark text-align-left">¿Estás seguro?</h4>
              </div>
              <div class="modal-body">
                  <p class="text-dark">¿Seguro que quieres borrar la categoria?</p>
                  <div class="col-12">
                      {{$categoria->nombre}}
                  </div>
                  <p class="text-warning"><small>Si la borras, nunca podrás recuperarla.</small></p>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                  <a href="{{url('/delete-categoria/'.$categoria->id)}}" class="btn btn-outline-light bg-danger">Eliminar</a>
              </div>
          </div>
      </div>
  </div>
  @endforeach

    
@endsection
