<!-- Bootstrap core CSS-->
<link href="{{ url('vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">


<!-- Page level plugin CSS-->
<link href="{{ url('vendor/datatables/dataTables.bootstrap4.css') }}" rel="stylesheet">

<!-- Custom styles for this template-->
<link href="{{ url('css/sb-admin.css') }}" rel="stylesheet">


  <div class="col-9">
    <h4>Productos en stock</h4>
  </div>
  <div class="col-12">
      <h6>Entregado a:___________________</h6>
    </div>

<div class="card">
  
  <div class="card-body">
    <div class="table-responsive">
      <table class="table table-bordered" id="dataTable" width="100%" >
        <thead>
          <tr>
            <th>No.</th>
            <th>Nombre</th>
            <th>Categoria</th>
            <th>Codigo</th>
            <th>Cantidad</th>
            <th>Pasillo</th>
            <th>Rack</th>
            <th>Nivel</th>
            <th>Responsable</th>
            
          </tr>
        </thead>
        <tbody>
          
          @foreach($productos as $producto)
          <tr>
            <td>{{$producto->id}}</td>
            <td>{{$producto->nombre}}</td>
            

            <td>
              {{$producto->categoria->nombre}}
            </td>

            <td>{{$producto->codigo}}</td>
            <td>
              {{$producto->cantidad}}
            </td>
            <td>{{$producto->pasillo}}</td>
            <td>{{$producto->rack}}</td>
            <td>{{$producto->nivel}}</td>
            <td>
              {{$producto->user->name}}
            </td>
            

          </tr>
          @endforeach

        </tbody>
      </table>
    </div>
  </div>
  
</div>

</div>

<!-- Bootstrap core JavaScript-->
<script src="{{ url('vendor/jquery/jquery.min.js') }}"></script>
<script src="{{ url('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

<!-- Core plugin JavaScript-->
<script src="{{ url('vendor/jquery-easing/jquery.easing.min.js') }}"></script>

<!-- Page level plugin JavaScript-->
<script src="{{ url('vendor/chart.js/Chart.min.js') }}"></script>
<script src="{{ url('vendor/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ url('vendor/datatables/dataTables.bootstrap4.js') }}"></script>

<!-- Custom scripts for all pages-->
<script src="{{ url('js/sb-admin.min.js') }}"></script>