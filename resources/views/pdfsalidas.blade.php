<!-- Bootstrap core CSS-->
<link href="{{ url('vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">


<!-- Page level plugin CSS-->
<link href="{{ url('vendor/datatables/dataTables.bootstrap4.css') }}" rel="stylesheet">

<!-- Custom styles for this template-->
<link href="{{ url('css/sb-admin.css') }}" rel="stylesheet">

<h3>Salidas Actuales</h3>
          
          
      <!-- DataTables Example -->
      <div class="card mb-3">
        
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>No.</th>
                  <th>Producto</th>
                  <th>Cantidad</th>
                  <th>Fecha de salida</th>
                  <th>Responsable</th>
                </tr>
              </thead>
              
              <tbody>
                
                @foreach($salidas as $salida)
                <tr>
                  <td>{{$salida->id}}</td>
                  <td>{{$salida->producto->nombre}}</td>
                  <td>{{$salida->cantidad}}</td>
                  <td>{{$salida->created_at}}</td>
                  <td>
                      {{$salida->user->name}}
                  </td>
                  
                </tr>
                @endforeach

              </tbody>
            </table>
          </div>
        </div>
      </div>

    
   

   